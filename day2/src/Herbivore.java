public abstract class Herbivore extends Animal{
    @Override
    public void eat(Food food) {
        if(food instanceof Grass) {
            this.hungry = false;
        }
        else
            System.out.println("Я такое не ем!");
    }
}
