import java.util.ArrayList;

public class Zoo {
    public static int sizeCarn = 3;
    public static int sizeHerb = 5;
    public Aviary aviaryCarn;
    public Aviary aviaryHerb;
    public Zoo(){
        this.aviaryCarn = new Aviary(sizeCarn);
        this.aviaryHerb = new Aviary(sizeHerb);
    }
    public static void main(String[] args) {
        Zoo zoo = new Zoo();
        Meat meat = new Meat();
        Grass grass = new Grass();
        Lion lion = new Lion();
        Eagle eagle = new Eagle();
        Tiger tiger = new Tiger();
        Duck duck = new Duck();
        Lemur lemur = new Lemur();
        Sheep sheep = new Sheep();


        lion.eat(grass);
        lion.eat(meat);
        lion.getHungry();

        zoo.aviaryCarn.addAnimal(lion);
        zoo.aviaryCarn.addAnimal(tiger);
        zoo.aviaryCarn.addAnimal(eagle);

        zoo.aviaryCarn.addAnimal(lion);// сообщение о переполнении вольера

      for  (Animal animal: zoo.aviaryCarn.animals){
animal.voice();
        }
        System.out.println("Перекличка хищников завершена!");
      zoo.aviaryHerb.addAnimal(duck);
      zoo.aviaryHerb.addAnimal(sheep);
      zoo.aviaryHerb.addAnimal(lemur);

      for  (Animal animal: zoo.aviaryHerb.animals){
            animal.voice();
        }
        System.out.println("Перекличка травоядных завершена!");
    }
}
