import java.util.ArrayList;

public class Aviary {
    private int size;
    public int count = 0;
    public ArrayList<Animal> animals;
    public Aviary(int size){

        this.size = size;
        animals = new ArrayList<>();
    }
    public int getSize(){
        return this.size;
    }
    public void addAnimal(Animal animal){
        if(this.count >= this.getSize())
            System.out.println("Вольер переполнен!");
        else {
            this.count++;
            animals.add(animal);
        }
    }
}
